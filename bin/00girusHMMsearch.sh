#!/bin/bash

#hmm search of query multi-faa against several marker hmms
#extraction of hits above threshold, removal of very short seqs
#concatenate with set of reference seqs for each marker
#preparation of qsub array script for tree construction with ete3 (align, tree)

# Mutlifaa as input
MULTIFA=$1
BASENAME=$(echo $MULTIFA | awk -F "/" '{print $NF}' | cut -f 1 -d '.')
FTYPE=$(echo $MULTIFA | awk -F "/" '{print $NF}' | awk -F "." '{print $NF}')
PATHTOMARKERS=/global/homes/f/fschulz/projectsgit/girusSC/hmm/

mkdir -p ${BASENAME}_hmmhits

echo "STARTING ANALYSIS $BASENAME"

# Perform gene calling if input is not multifaa
if [ $FTYPE != "faa" ]; then
	echo "Input is not in multifaa, starting gene calling"
    $HOME/bin/prodigal -i $MULTIFA -a ${BASENAME}.faa -q -p meta > /dev/null
else
	echo "Input is in multifaa"
fi
if [ $? == 0 ]; then
    echo "success"
else
    echo "gene calling failed $?"
    exit
fi


# Use hmms to search against query and reference sequences
echo "Started hmm search"
for x in ${PATHTOMARKERS}*hmm; do
   ~/bin/hmmsearch --noali --cut_tc --domtblout ${BASENAME}_hmmhits/$(echo $x | awk -F "/" '{print $NF}' | awk -F "." '{print $1}')_${BASENAME}_hmmHits.txt $x ${BASENAME}.faa > /dev/null
done
if [ $? == 0 ]; then 
	echo "success"
else
	echo "failed hmmsearch $?"
	exit
fi


i=0
nh=0
for x in ${BASENAME}_hmmhits/*${BASENAME}_hmmHits.txt; do
    MARKER=$(echo $x | awk -F "/" '{print $NF}' | awk -F "_" '{print $1}')
	i=$((i+1))
    awk '{if ($0 !~ /^#/) print $1 }' $x | awk '!x[$0]++' > ${BASENAME}_hmmhits/${MARKER}_${BASENAME}.sighits
	if [ -s ${BASENAME}_hmmhits/${MARKER}_${BASENAME}.sighits ]; then
		NOHITS=$(wc -l ${BASENAME}_hmmhits/${MARKER}_${BASENAME}.sighits | awk '{print $1}')
		echo "$NOHITS hits for $MARKER"
	else
		echo "No hits for $MARKER"
		nh=$((nh+1))
	fi
done

# Stop if there are no hits
if [ "$i" == "$nh" ]; then
	echo "No hits in $MULTIFA"
	echo "Exit"
	exit
fi

echo "HMMSEARCH DONE"


#extract contigs which contain gene with hits against HMM
cat ${BASENAME}_hmmhits/*_${BASENAME}.sighits | awk '{print $1}' | awk -F "_" 'BEGIN{OFS="_";}{$NF=""; print $0}' | sed 's/_$//g' | awk '!x[$0]++' >> ${BASENAME}_girus_contigs.tab


#cleanup
find ${BASENAME}_hmmhits/*${BASENAME}* -size 0 -type f -delete

echo "EXTRACT"

#extraction based on filetype
if [ -s ${BASENAME}.fasta ]
then
    python $HOME/bin/extractseq_byID.py ${BASENAME}_girus_contigs.tab ${BASENAME}.fasta
elif [ -s ${BASENAME}.fna ]
then
    python $HOME/bin/extractseq_byID.py ${BASENAME}_girus_contigs.tab ${BASENAME}.fna
elif [ -s ${BASENAME}.faa ]
then
    python $HOME/bin/extractseq_byID.py ${BASENAME}_girus_contigs.tab ${BASENAME}.faa
else
    python $HOME/bin/extractseq_byID.py ${BASENAME}_girus_contigs.tab ${BASENAME}.fa
fi

#extraction of best hits as faa
if [ "$(ls ${BASENAME}_hmmhits/*${BASENAME}.sighits | wc -l)" -gt 1 ]
then
    for x in ${BASENAME}_hmmhits/*${BASENAME}.sighits; do
        python $HOME/bin/extractseq_byID.py $x ${BASENAME}.faa
    done
fi

echo "DONE"
