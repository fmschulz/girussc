import sys
from Bio import SeqIO

seqsids = sys.argv[1]
multifasta = sys.argv[2]

outfile = open(str(multifasta).split("/")[-1].split(".")[0] + ".extracted.fasta", "w")

def extractseq (seqid, multifasta):
    for seq_record in SeqIO.parse(multifasta, "fasta"):
        #if str(seq_record.description).split("|")[0] == seqid:
        if seq_record.description == seqid:
            outfile.write(">" + str(seq_record.description).split()[0] + "\n" + str(seq_record.seq).replace("*","") + "\n")
        else:
            pass

with open(seqsids, "r") as infile:
    for line in infile:
        line = line.strip()
        extractseq(line, multifasta)

outfile.close()
